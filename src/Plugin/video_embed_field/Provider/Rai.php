<?php
namespace Drupal\video_embed_rai\Plugin\video_embed_field\Provider;
use Drupal\video_embed_field\ProviderPluginBase;
/**
 * A Rai.tv provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "rai",
 *   title = @Translation("Rai")
 * )
 */
class Rai extends ProviderPluginBase {
  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $embed_code = [
      '#type' => 'video_embed_iframe',
      '#provider' => 'rai',
      '#url' => sprintf('http://www.rai.it/dl/RaiTV/programmi/media/%s.html', $this->getVideoId()),
      '#query' => [
        'iframe' => 1,
        'autoplay' => ($autoplay) ? 'true' : 'false',
        'rel' => '0',
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
    return $embed_code;
  }
  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $url = sprintf('http://www.rai.it/dl/RaiTV/programmi/media/%s.html?json', $this->getVideoId());
    $data = json_decode(file_get_contents($url));
    if (property_exists ($data, "imageLarge")){
      if (preg_match("/^\/dl/", $data->imageLarge))
          $data->imageLarge = "http://rai.it" . $data->imageLarge;
      return $data->imageLarge;
    }
    if (property_exists ($data, "images" ))
      return str_replace ( "[RESOLUTION]", "300x300" , $data->images->landscape);
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match("/^http:\/\/(?:www\.(?:raiplay.it\/video|(?:rai.(it|tv)\/(?:raiplay|dl)))).*\/(?<id>[0-9A-Za-z_-]*)\.html/",  $input, $matches);
    if (isset($matches['id'])) {
      $data = json_decode(file_get_contents($matches[0]."?json"));
      if (property_exists ($data, "ID")) return $data->ID;
      if (property_exists ($data, "itemId")) return $data->itemId;
    } else return FALSE;
  }
}
